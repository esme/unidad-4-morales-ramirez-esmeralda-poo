﻿using FarmaciaMQE.COMMON;
using FarmaciaMQE.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarmaciaMQE.BIZ
{
    public class ManejadorDeClientes:IManejadorDeClientes
    {
		IRepositorio<Cliente> repositorio;
		public ManejadorDeClientes(IRepositorio<Cliente> repositorio)
		{
			this.repositorio = repositorio;
		}

		public List<Cliente> Listar => repositorio.Read;

		public bool Agregar(Cliente entidad)
		{
			return repositorio.Create(entidad);
		}

		public Cliente BuscarPorId(string id)
		{
			return Listar.Where(e => e.Id == id).SingleOrDefault();
		}

		public bool Eliminar(string id)
		{
			return repositorio.Delete(id);
		}

		public bool Modificar(Cliente entidad)
		{
			return repositorio.Update(entidad);
		}
	}
}
