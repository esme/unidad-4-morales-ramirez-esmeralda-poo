﻿using FarmaciaMQE.COMMON;
using FarmaciaMQE.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarmaciaMQE.BIZ
{
	public class ManejadorDeMedicamentos : IManejadorDeMedicamentos
    {
		IRepositorio<Medicamento> repositorio;
		public ManejadorDeMedicamentos(IRepositorio<Medicamento> repositorio)
		{
			this.repositorio = repositorio;
		}

		public List<Medicamento> Listar => repositorio.Read;

		public bool Agregar(Medicamento entidad)
		{
			return repositorio.Create(entidad);
		}

		public Medicamento BuscarPorId(string id)
		{
			return Listar.Where(e => e.Id == id).SingleOrDefault();
		}

		public bool Eliminar(string id)
		{
			return repositorio.Delete(id);
		}

		public bool Modificar(Medicamento entidad)
		{
			return repositorio.Update(entidad);
		}
	}
}
