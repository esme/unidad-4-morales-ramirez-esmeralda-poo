﻿using FarmaciaMQE.COMMON;
using FarmaciaMQE.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarmaciaMQE.BIZ
{
    public class ManejadorDeEmpleados: IManejadorDeEmpleados
    {
		IRepositorio<Empleado> repositorio;
		public ManejadorDeEmpleados(IRepositorio<Empleado> repositorio)
		{
			this.repositorio = repositorio;
		}

		public List<Empleado> Listar => repositorio.Read;

		public bool Agregar(Empleado entidad)
		{
			return repositorio.Create(entidad);
		}

		public Empleado BuscarPorId(string id)
		{
			return Listar.Where(e => e.Id == id).SingleOrDefault();
		}

		public bool Eliminar(string id)
		{
			return repositorio.Delete(id);
		}

		public bool Modificar(Empleado entidad)
		{
			return repositorio.Update(entidad);
		}
	}
}
