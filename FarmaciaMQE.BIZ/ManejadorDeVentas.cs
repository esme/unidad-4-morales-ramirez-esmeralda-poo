﻿using FarmaciaMQE.COMMON.Entidades;
using FarmaciaMQE.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarmaciaMQE.BIZ
{
    public class ManejadorDeVentas:IManejadorDeVentas
    {
		IRepositorio<Venta> repositorio;
		public ManejadorDeVentas(IRepositorio<Venta> repositorio)
		{
			this.repositorio = repositorio;
		}

		public List<Venta> Listar => repositorio.Read;

		public bool Agregar(Venta entidad)
		{
			return repositorio.Create(entidad);
		}

		public Venta BuscarPorId(string id)
		{
			return Listar.Where(e => e.Id == id).SingleOrDefault();
		}

		public bool Eliminar(string id)
		{
			return repositorio.Delete(id);
		}

		public bool Modificar(Venta entidad)
		{
			return repositorio.Update(entidad);
		}
	}
}
