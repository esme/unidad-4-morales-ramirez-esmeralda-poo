﻿using FarmaciaMQE.COMMON;
using FarmaciaMQE.COMMON.Interfaces;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmaciaMQE.BIZ
{
	public class ManejadorDeCategorias : IManejadorDeCategorias
	{
		IRepositorio<Categoria> repositorio;
		public ManejadorDeCategorias(IRepositorio<Categoria> repositorio)
		{
			this.repositorio = repositorio;
		}

		public List<Categoria> Listar => repositorio.Read;

		public bool Agregar(Categoria entidad)
		{
			return repositorio.Create(entidad);
		}

		public Categoria BuscarPorId(string id)
		{
			return Listar.Where(e => e.Id == id).SingleOrDefault();
		}

		public bool Eliminar(string id)
		{
			return repositorio.Delete(id);
		}

		public bool Modificar(Categoria entidad)
		{
			return repositorio.Update(entidad);
		}
	}
}
