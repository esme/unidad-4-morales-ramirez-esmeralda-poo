﻿using FarmaciaMQE.BIZ;
using FarmaciaMQE.COMMON;
using FarmaciaMQE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para NuevoEmpleado.xaml
	/// </summary>
	public partial class NuevoEmpleado : Window
	{
		ManejadorDeEmpleados manejadorDeEmpleados;
		enum Accion
		{
			Nuevo,
			Editar,
		}
		Accion accionCategoria;
		Empleado c = new Empleado();
		public NuevoEmpleado(int AccionElemento, Empleado c)
		{
			InitializeComponent();
			txbNombreEmpleado.Clear();
			txbPuesto.Clear();
			txbTelefono.Clear();
			txbApellidoMaterno.Clear();
			txbApellidoPaterno.Clear();
			manejadorDeEmpleados = new ManejadorDeEmpleados(new RepositorioDeEmpleados());
			switch (AccionElemento)
			{
				case 1:
					accionCategoria = Accion.Nuevo;
					btnEditar.IsEnabled = false;

					break;
				case 2:
					btnEditar.IsEnabled = true;
					btnCancelar.IsEnabled = true;
					btnLimpiarCampos.IsEnabled = false;
					btnGuardar.IsEnabled = false;
					txbApellidoMaterno.Text=c.ApellidoMaterno;
					txbApellidoPaterno.Text=c.ApellidoPaterno;
					txbNombreEmpleado.Text=c.Nombre;
					txbPuesto.Text=c.Puesto;
					txbTelefono.Text=c.Telefono;
					txbApellidoMaterno.IsEnabled = false;
					txbApellidoPaterno.IsEnabled = false;
					txbNombreEmpleado.IsEnabled = false;
					txbPuesto.IsEnabled = false;
					txbTelefono.IsEnabled = false;
					this.c = c;
					break;
			}
		}

		private void btnLimpiarCampos_Click(object sender, RoutedEventArgs e)
		{
			txbNombreEmpleado.Clear();
			txbPuesto.Clear();
			txbTelefono.Clear();
			txbApellidoMaterno.Clear();
			txbApellidoPaterno.Clear();
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			Catalogos VentanaCatalogos = new Catalogos();
			VentanaCatalogos.Show();
			Close();
		}

		private void btnGuardar_Click(object sender, RoutedEventArgs e)
		{
			if (accionCategoria == Accion.Nuevo)
			{
				Empleado empleado = new Empleado()
				{
					ApellidoMaterno=txbApellidoMaterno.Text,
					ApellidoPaterno =txbApellidoPaterno.Text,
					Nombre= txbNombreEmpleado.Text,
					Puesto= txbPuesto.Text,
					Telefono= txbTelefono.Text,
			};
				if (manejadorDeEmpleados.Agregar(empleado))
				{
					MessageBox.Show("Empleado agregado con éxito", "Nuevo Empleado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al agregar Empleado ", "Nuevo Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{
				c.ApellidoMaterno = txbApellidoMaterno.Text;
				c.ApellidoPaterno = txbApellidoPaterno.Text;
				c.Nombre = txbNombreEmpleado.Text;
				c.Puesto = txbPuesto.Text;
				c.Telefono = txbTelefono.Text;
				if (manejadorDeEmpleados.Modificar(c))
				{
					MessageBox.Show("Empleado modificado con éxito", "Editar Empleado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al modificar Empleado", "Editar Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}

		}

		private void btnEditar_Click(object sender, RoutedEventArgs e)
		{
			accionCategoria = Accion.Editar;
			btnEditar.IsEnabled = false;
			btnCancelar.IsEnabled = true;
			btnLimpiarCampos.IsEnabled = true;
			btnGuardar.IsEnabled = true;
			txbApellidoMaterno.IsEnabled = true;
			txbApellidoPaterno.IsEnabled = true;
			txbNombreEmpleado.IsEnabled = true;
			txbPuesto.IsEnabled = true;
			txbTelefono.IsEnabled = true;
		}
	}
}
