﻿using FarmaciaMQE.BIZ;
using FarmaciaMQE.COMMON;
using FarmaciaMQE.COMMON.Entidades;
using FarmaciaMQE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para NuevaVenta.xaml
	/// </summary>
	public partial class NuevaVenta : Window
	{
		ManejadorDeClientes manejadorDeClientes;
		ManejadorDeEmpleados manejadorDeEmpleados;
		ManejadorDeMedicamentos manejadorDeMedicamentos;
		ManejadorDeVentas manejadorDeVentas;
		enum Accion
		{
			Nuevo,
			Editar,
		}
		Accion accionCategoria;
		Venta c;
		Articulo x = new Articulo();
		
		public NuevaVenta(int AccionElemento, Venta c)
		{
			InitializeComponent();
			manejadorDeVentas = new ManejadorDeVentas(new RepositorioDeVentas());
			manejadorDeClientes = new ManejadorDeClientes(new RepositorioDeClientes());
			manejadorDeEmpleados = new ManejadorDeEmpleados(new RepositorioDeEmpleados());
			manejadorDeMedicamentos = new ManejadorDeMedicamentos(new RepositorioDeMedicamentos());
			cmbCliente.ItemsSource = null;
			cmbCliente.ItemsSource = manejadorDeClientes.Listar;
			cmbEmpleados.ItemsSource = null;
			cmbEmpleados.ItemsSource = manejadorDeEmpleados.Listar;
			cmbMedicamento.ItemsSource = null;
			cmbMedicamento.ItemsSource = manejadorDeMedicamentos.Listar;
			switch (AccionElemento)
			{
				case 1:
					accionCategoria = Accion.Nuevo;
					this.c = new Venta();
					break;
				case 2:
					btnEditar.IsEnabled = true;
					btnCancelar.IsEnabled = true;
					btnSeleccionarCliente.IsEnabled = false;
					btnSeleccionarVendedor.IsEnabled = false;
					btnGuardar.IsEnabled = false;
					btnEliminar.IsEnabled = false;
					cmbCliente.SelectedItem = c.Comprador;
					cmbEmpleados.Text = c.Vendedeor;
					cmbCliente.IsEnabled = false;
					cmbEmpleados.IsEnabled = false;
					dtgProductos.ItemsSource = null;
					dtgProductos.ItemsSource = c.Productos;
					this.c = c;
					break;
			}
			this.c.Productos = new List<Medicamento>();
		}

		private void btnSeleccionarVendedor_Click(object sender, RoutedEventArgs e)
		{
			cmbEmpleados.IsEnabled = false;
		}

		private void btnSeleccionarCliente_Click(object sender, RoutedEventArgs e)
		{
			cmbCliente.IsEnabled = false;
		}

		private void btnEditar_Click(object sender, RoutedEventArgs e)
		{
			cmbCliente.IsEnabled = true;
			cmbEmpleados.IsEnabled = true;
			cmbMedicamento.IsEnabled = true;
			btnEliminar.IsEnabled = true;
			btnGuardar.IsEnabled = true;
		}
		private void btnEliminar_Click(object sender, RoutedEventArgs e)
		{
			if (c.Productos.Count() == 0)
			{
				MessageBox.Show("Aun no existen Productos para eliminar", "Eliminar Producto", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				Medicamento m = cmbMedicamento.SelectedItem as Medicamento;
				c.Productos.Remove(m);
				dtgProductos.ItemsSource = null;
				dtgProductos.ItemsSource = c.Productos;
			}
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			Ventas VentanaVentas = new Ventas();
			VentanaVentas.Show();
			Close();
		}

		private void btnAgregarArticulo_Click(object sender, RoutedEventArgs e)
		{
			Medicamento m = cmbMedicamento.SelectedItem as Medicamento;
	
			c.Productos.Add(m);
			c.Total = ObtenerTotal();
			txbTotal.Text = c.Total.ToString();
			dtgProductos.ItemsSource = null;
			dtgProductos.ItemsSource = c.Productos;
		}

		private double ObtenerTotal()
		{
			double suma = 0;
			foreach (var item in c.Productos)
			{
				suma += double.Parse(item.PrecioVenta);
			}
			return suma;
		}

		private void btnGuardar_Click(object sender, RoutedEventArgs e)
		{
			if (accionCategoria == Accion.Nuevo)
			{
				Venta venta = new Venta()
				{
					Fecha = DateTime.Now,
					Comprador = cmbCliente.Text,
					Vendedeor = cmbEmpleados.Text,
					Productos = c.Productos,
					Total=c.Total

				};
				if (manejadorDeVentas.Agregar(venta))
				{
					MessageBox.Show("Venta agregado con éxito", "Nuevo Venta", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Ventas Principal = new Ventas();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al agregar Venta", "Nueva Venta", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{
				c.Fecha = DateTime.Now;
				c.Comprador = cmbCliente.Text;
				c.Vendedeor = cmbEmpleados.Text;
				c.Productos = c.Productos;
				if (manejadorDeVentas.Modificar(c))
				{
					MessageBox.Show("Venta modificada con éxito", "Editar Venta", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Ventas Principal = new Ventas();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al modificar Ventas", "Editar Venta", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}

		}
	}
}
