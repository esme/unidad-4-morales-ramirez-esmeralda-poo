﻿using FarmaciaMQE.BIZ;
using FarmaciaMQE.COMMON;
using FarmaciaMQE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para NuevaCategoria.xaml
	/// </summary>
	public partial class NuevaCategoria : Window
	{
		ManejadorDeCategorias manejadorDeCategorias;
		enum Accion
		{
			Nuevo,
			Editar,
		}
		Accion accionCategoria;
		Categoria c = new Categoria();
		public NuevaCategoria(int AccionElemento, Categoria c)
		{
			InitializeComponent();
			txbDescripcion.Clear();
			txbNombreCategoria.Clear();
			manejadorDeCategorias = new ManejadorDeCategorias(new RepositorioDeCategorias());
			switch (AccionElemento)
			{
				case 1:
					accionCategoria = Accion.Nuevo;
					btnEditar.IsEnabled = false;

					break;
				case 2:
					btnEditar.IsEnabled = true;
					btnCancelar.IsEnabled = true;
					btnLimpiar.IsEnabled = false;
					btnGuardarCat.IsEnabled = false;
					txbNombreCategoria.Text = c.Nombre;
					txbDescripcion.Text = c.Descripcion;
					txbNombreCategoria.IsEnabled = false;
					txbDescripcion.IsEnabled = false;
					this.c = c;
					break;

			}
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			Catalogos VentanaCatalogos = new Catalogos();
			VentanaCatalogos.Show();
			Close();
		}

		private void btnLimpiar_Click(object sender, RoutedEventArgs e)
		{
			txbDescripcion.Clear();
			txbNombreCategoria.Clear();
		}

		private void btnGuardarCat_Click(object sender, RoutedEventArgs e)
		{
			if (accionCategoria == Accion.Nuevo)
			{
				Categoria categoria = new Categoria()
				{
					Nombre = txbNombreCategoria.Text,
					Descripcion = txbDescripcion.Text,
				};
				if (manejadorDeCategorias.Agregar(categoria))
				{
					MessageBox.Show("Categoría agregada con éxito", "Nueva Categoría", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al agregar Categoria ", "Nueva Categoría", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{
				c.Nombre = txbNombreCategoria.Text;
				c.Descripcion = txbDescripcion.Text;
				if (manejadorDeCategorias.Modificar(c))
				{
					MessageBox.Show("Categoría modificada con éxito", "Editar Categoría", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					 MessageBox.Show("Error al guardar Categoría", "Editar Categoría", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
		}

		private void btnEditar_Click(object sender, RoutedEventArgs e)
		{
			accionCategoria = Accion.Editar;
			btnEditar.IsEnabled = false;
			btnCancelar.IsEnabled = true;
			btnLimpiar.IsEnabled = true;
			btnGuardarCat.IsEnabled = true;
			txbNombreCategoria.IsEnabled = true;
			txbDescripcion.IsEnabled = true;
		}
	}
}
