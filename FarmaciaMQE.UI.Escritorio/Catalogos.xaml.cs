﻿using FarmaciaMQE.BIZ;
using FarmaciaMQE.COMMON;
using FarmaciaMQE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para Catalogos.xaml
	/// </summary>
	public partial class Catalogos : Window
	{
		ManejadorDeCategorias manejadorDeCategorias;
		ManejadorDeClientes manejadorDeClientes;
		ManejadorDeEmpleados manejadorDeEmpleados;
		ManejadorDeMedicamentos manejadorDeMedicamentos;
		int accionElemento;
		public Catalogos()
		{
			InitializeComponent();
			manejadorDeEmpleados = new ManejadorDeEmpleados(new RepositorioDeEmpleados());
			manejadorDeCategorias = new ManejadorDeCategorias(new RepositorioDeCategorias());
			manejadorDeClientes = new ManejadorDeClientes(new RepositorioDeClientes());
			manejadorDeMedicamentos = new ManejadorDeMedicamentos(new RepositorioDeMedicamentos());
			ActualizarTablaDeCategorias();
			ActualizarTablaDeClientes();
			ActualizarTablaDeEmpleados();
			ActualizarTablaDeMedicamentos();

		}
		private void ActualizarTablaDeMedicamentos()
		{
			dtgMedicamento.ItemsSource = null;
			dtgMedicamento.ItemsSource = manejadorDeMedicamentos.Listar;
		}

		private void ActualizarTablaDeEmpleados()
		{
			dtgEmpleados.ItemsSource = null;
			dtgEmpleados.ItemsSource = manejadorDeEmpleados.Listar;
		}

		private void ActualizarTablaDeClientes()
		{
			dtgClientes.ItemsSource = null;
			dtgClientes.ItemsSource = manejadorDeClientes.Listar;
		}

		private void ActualizarTablaDeCategorias()
		{
			dtgCategorias.ItemsSource = null;
			dtgCategorias.ItemsSource = manejadorDeCategorias.Listar;
		}

		private void btnRegresar_Click(object sender, RoutedEventArgs e)
		{
			RegresarVentanaPrincipal();
		}

		private void RegresarVentanaPrincipal()
		{
			MainWindow Principal = new MainWindow();
			Principal.Show();
			Close();
		}

		private void btnNuevaCategoria_Click(object sender, RoutedEventArgs e)
		{
			accionElemento = 1;
			Categoria c=new Categoria();
			NuevaCategoria nuevaCategoria = new NuevaCategoria(accionElemento, c);
			nuevaCategoria.Show(); 
			Close();
		}

		private void btnRegresar_Click_1(object sender, RoutedEventArgs e)
		{
			RegresarVentanaPrincipal();
		}

		private void btnNuevoCliente_Click(object sender, RoutedEventArgs e)
		{
			accionElemento = 1;
			Cliente c = new Cliente();
			NuevoCliente nuevoCliente = new NuevoCliente(accionElemento, c);
			nuevoCliente.Show();
			Close();
		}

		private void btnRegresarCliente_Click(object sender, RoutedEventArgs e)
		{
			RegresarVentanaPrincipal();
		}
		private void btnRegresarEmpleado_Click(object sender, RoutedEventArgs e)
		{
			RegresarVentanaPrincipal();
		}

		private void btnNuevoEmpleado_Click(object sender, RoutedEventArgs e)
		{
			accionElemento = 1;
			Empleado c = new Empleado();
			NuevoEmpleado nuevoEmpleado = new NuevoEmpleado(accionElemento, c);
			nuevoEmpleado.Show();
			Close();
		}
		private void btnRegresarMedicamento_Click(object sender, RoutedEventArgs e)
		{
			RegresarVentanaPrincipal();
		}

		private void btnNuevoMedicamento_Click(object sender, RoutedEventArgs e)
		{
			accionElemento = 1;
			Medicamento c = new Medicamento();
			NuevoMedicamento nuevoMedicamento = new NuevoMedicamento(accionElemento, c);
			nuevoMedicamento.Show();
			Close();
		}

		private void btnEliminarCategoria_Click(object sender, RoutedEventArgs e)
		{
			if (manejadorDeCategorias.Listar.Count() == 0)
			{
				MessageBox.Show("Aun no existen Categorias para eliminar", "Eliminar Categoría", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgCategorias.SelectedItem != null)
				{
					Categoria a = dtgCategorias.SelectedItem as Categoria;
					if (MessageBox.Show("Realmente deseas remover de manera permanente la Categoría " + a.Nombre + "?", "Eliminar Categoría", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (manejadorDeCategorias.Eliminar(a.Id))
						{
							MessageBox.Show("La categoría fue Eliminada", "Eliminar Categoría", MessageBoxButton.OK, MessageBoxImage.Information);
							ActualizarTablaDeCategorias();						}
						else
						{
							MessageBox.Show("Error al eliminar la categoria.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Aún no selecciona la Categoría a eliminar", "Eliminar Categoría", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}

		}

		private void btnEliminarCliente_Click(object sender, RoutedEventArgs e)
		{
			if (manejadorDeClientes.Listar.Count() == 0)
			{
				MessageBox.Show("Aun no existen elementos de tipo Cliente para eliminar", "Eliminar Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgClientes.SelectedItem != null)
				{
					Cliente a = dtgClientes.SelectedItem as Cliente;
					if (MessageBox.Show("Realmente deseas remover de manera permanente al Cliente " + a.Nombre + "?", "Eliminar Cliente", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (manejadorDeClientes.Eliminar(a.Id))
						{
							MessageBox.Show("El cliente fue Eliminado", "Eliminar Cliente", MessageBoxButton.OK, MessageBoxImage.Information);
							ActualizarTablaDeClientes();
						}
						else
						{
							MessageBox.Show("Error al eliminar el Cliente.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Aún no selecciona el Cliente que desea eliminar", "Eliminar Cliente", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}

		}

		private void btnEliminarEmpleado_Click(object sender, RoutedEventArgs e)
		{
			if (manejadorDeEmpleados.Listar.Count() == 0)
			{
				MessageBox.Show("Aun no existen Empleados para eliminar", "Eliminar Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Empleado a = dtgEmpleados.SelectedItem as Empleado;
					if (MessageBox.Show("Realmente deseas remover de manera permanente al Empleado " + a.Nombre + "?", "Eliminar Empleado", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (manejadorDeEmpleados.Eliminar(a.Id))
						{
							MessageBox.Show("El Empleado fue Eliminado", "Eliminar Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
							ActualizarTablaDeEmpleados();
						}
						else
						{
							MessageBox.Show("Error al eliminar el elemento de tipo Empleado.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Aún no selecciona el Empleado que desea eliminar", "Eliminar Empleado", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}

		private void btnEliminarMedicamento_Click(object sender, RoutedEventArgs e)
		{
			if (manejadorDeMedicamentos.Listar.Count() == 0)
			{
				MessageBox.Show("Aun no existen Medicamentos para eliminar", "Eliminar Medicamento", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgMedicamento.SelectedItem != null)
				{
					Medicamento a = dtgMedicamento.SelectedItem as Medicamento;
					if (MessageBox.Show("Realmente deseas remover de manera permanente el Medicamento " + a.Nombre + "?", "Eliminar Medicamento", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (manejadorDeMedicamentos.Eliminar(a.Id))
						{
							MessageBox.Show("El Medicamento fue Eliminado", "Eliminar Medicamento", MessageBoxButton.OK, MessageBoxImage.Information);
							ActualizarTablaDeMedicamentos();
						}
						else
						{
							MessageBox.Show("Error al eliminar el Medicamento", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Aún no selecciona el Medicamento que desea eliminar", "Eliminar Medicamento", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}

		private void dtgCategorias_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			Categoria c = dtgCategorias.SelectedItem as Categoria;
			accionElemento = 2;
			NuevaCategoria nuevaCategoria = new NuevaCategoria(accionElemento, c);
			nuevaCategoria.Show();
			Close();

		}

		private void dtgClientes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			Cliente c = dtgClientes.SelectedItem as Cliente;
			accionElemento = 2;
			NuevoCliente nuevoCliente = new NuevoCliente(accionElemento, c);
			nuevoCliente.Show();
			Close();
		}

		private void dtgEmpleados_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			Empleado c = dtgEmpleados.SelectedItem as Empleado;
			accionElemento = 2;
			NuevoEmpleado nuevoEmpleado = new NuevoEmpleado(accionElemento, c);
			nuevoEmpleado.Show();
			Close();
		}

		private void dtgMedicamento_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			Medicamento c = dtgMedicamento.SelectedItem as Medicamento;
			accionElemento = 2;
			NuevoMedicamento nuevoMedicamento = new NuevoMedicamento(accionElemento, c);
			nuevoMedicamento.Show();
			Close();
		}
	}
}
