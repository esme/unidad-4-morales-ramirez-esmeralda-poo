﻿using FarmaciaMQE.BIZ;
using FarmaciaMQE.COMMON;
using FarmaciaMQE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para NuevoMedicamento.xaml
	/// </summary>
	public partial class NuevoMedicamento : Window
	{
		ManejadorDeMedicamentos manejadorDeMedicamentos;
		ManejadorDeCategorias manejadorDeCategorias;
		enum Accion
		{
			Nuevo,
			Editar,
		}
		Accion accionCategoria;
		Medicamento c = new Medicamento();
		public NuevoMedicamento(int AccionElemento, Medicamento c)
		{
			InitializeComponent();
			manejadorDeCategorias = new ManejadorDeCategorias(new RepositorioDeCategorias());
			manejadorDeMedicamentos = new ManejadorDeMedicamentos(new RepositorioDeMedicamentos());
			txbDescripcion.Clear();
			txbNombreMedicamento.Clear();
			txbPrecioCompra.Clear();
			txbPrecioVenta.Clear();
			txbPresentacion.Clear();
			cmbCategorias.ItemsSource = null;
			cmbCategorias.ItemsSource = manejadorDeCategorias.Listar;
			switch (AccionElemento)
			{
				case 1:
					accionCategoria = Accion.Nuevo;
					btnEditar.IsEnabled = false;
					break;
				case 2:
					btnEditar.IsEnabled = true;
					btnCancelar.IsEnabled = true;
					btnLimpiar.IsEnabled = false;
					btnSeleccionar.IsEnabled = false;
					cmbCategorias.IsEnabled = false;
					btnGuardar.IsEnabled = false;
					txbDescripcion.Text = c.Descripcion;
					txbNombreMedicamento.Text = c.Nombre;
					txbPrecioCompra.Text = c.PrecioCompra;
					txbPrecioVenta.Text = c.PrecioVenta;
					txbPresentacion.Text = c.Presentacion;
					cmbCategorias.SelectedItem = c.Categoria;
					cmbCategorias.Text = c.Categoria.ToString();
					txbDescripcion.IsEnabled=false;
					txbNombreMedicamento.IsEnabled=false;
					txbPrecioCompra.IsEnabled=false;
					txbPrecioVenta.IsEnabled=false;
					txbPresentacion.IsEnabled = false;
					this.c = c;
					break;
			}

			}

		private void btnLimpiar_Click(object sender, RoutedEventArgs e)
		{
			txbDescripcion.Clear();
			txbNombreMedicamento.Clear();
			txbPrecioCompra.Clear();
			txbPrecioVenta.Clear();
			txbPresentacion.Clear();
			cmbCategorias.IsEnabled = true;
			cmbCategorias.ItemsSource = null;
		}

		private void btnSeleccionar_Click(object sender, RoutedEventArgs e)
		{
			cmbCategorias.IsEnabled = false;
		}

		private void btnGuardar_Click(object sender, RoutedEventArgs e)
		{
			if (accionCategoria == Accion.Nuevo)
			{
				Medicamento medicamento = new Medicamento()
				{
					Nombre=txbNombreMedicamento.Text,
					Descripcion = txbDescripcion.Text,
					Presentacion=txbPresentacion.Text,
					Categoria=cmbCategorias.SelectedItem as Categoria,
					PrecioCompra=txbPrecioCompra.Text,
					PrecioVenta=txbPrecioVenta.Text,
				};
				if (manejadorDeMedicamentos.Agregar(medicamento))
				{
					MessageBox.Show("Medicamento agregado con éxito", "Nuevo Medicamento", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al agregar Medicamento", "Nuevo Medicamento", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{
				c.Nombre = txbNombreMedicamento.Text;
				c.Descripcion = txbDescripcion.Text;
				c.Presentacion = txbPresentacion.Text;
				c.Categoria = cmbCategorias.SelectedItem as Categoria;
				c.PrecioCompra = txbPrecioCompra.Text;
				c.PrecioVenta = txbPrecioVenta.Text;
			if (manejadorDeMedicamentos.Modificar(c))
				{
					MessageBox.Show("Medicamento modificado con éxito", "Editar Medicamento", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al modificar Medicamento", "Editar Medicamento", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}

		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			Catalogos VentanaCatalogos = new Catalogos();
			VentanaCatalogos.Show();
			Close();
		}

		private void btnEditar_Click(object sender, RoutedEventArgs e)
		{
			accionCategoria = Accion.Editar;
			btnEditar.IsEnabled = false;
			btnCancelar.IsEnabled = true;
			btnLimpiar.IsEnabled = true;
			btnGuardar.IsEnabled = true;
			txbDescripcion.IsEnabled = true;
			txbNombreMedicamento.IsEnabled = true;
			txbPrecioCompra.IsEnabled = true;
			txbPrecioVenta.IsEnabled = true;
			txbPresentacion.IsEnabled = true;
			cmbCategorias.IsEnabled = true;
			btnSeleccionar.IsEnabled = true;

		}
	}
}
