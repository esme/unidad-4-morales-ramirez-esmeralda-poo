﻿using FarmaciaMQE.BIZ;
using FarmaciaMQE.COMMON.Entidades;
using FarmaciaMQE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para Ventas.xaml
	/// </summary>
	public partial class Ventas : Window
	{
		ManejadorDeVentas manejadorDeVentas;
		int accionElemento;
		public Ventas()
		{
			InitializeComponent();
			manejadorDeVentas = new ManejadorDeVentas(new RepositorioDeVentas());
			txbFecha.IsEnabled=false;
			txbFecha.Text = DateTime.Now.ToShortDateString();
			ActualizarTablaDeVentas();
		}

		private void ActualizarTablaDeVentas()
		{
			dtgVentas.ItemsSource = null;
			dtgVentas.ItemsSource = manejadorDeVentas.Listar;
		}

		private void dtgVentas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			Venta c = dtgVentas.SelectedItem as Venta;
			accionElemento = 2;
			NuevaVenta nuevaVenta = new NuevaVenta(accionElemento, c);
			nuevaVenta.Show();
			Close();
		}

		private void btnRegresar_Click(object sender, RoutedEventArgs e)
		{
			MainWindow Principal = new MainWindow();
			Principal.Show();
			Close();
		}

		private void btnNuevaVenta_Click(object sender, RoutedEventArgs e)
		{
			accionElemento = 1;
			Venta c = new Venta();
			NuevaVenta nuevaVenta = new NuevaVenta(accionElemento, c);
			nuevaVenta.Show();
			Close();
		}

		private void btnEliminarVenta_Click(object sender, RoutedEventArgs e)
		{
			if (manejadorDeVentas.Listar.Count() == 0)
			{
				MessageBox.Show("Aun no existen Ventas para eliminar", "Eliminar Venta", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgVentas.SelectedItem != null)
				{
					Venta a = dtgVentas.SelectedItem as Venta;
					if (MessageBox.Show("Realmente deseas remover de manera permanente la Venta?", "Eliminar Venta", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{	
							if (manejadorDeVentas.Eliminar(a.Id))
							{
								MessageBox.Show("La venta fue Eliminada", "Eliminar Venta", MessageBoxButton.OK, MessageBoxImage.Information);
								ActualizarTablaDeVentas();
							}
							else
							{
								MessageBox.Show("Error al eliminar la venta.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
							}
					}
				}
				else
				{
					MessageBox.Show("Aún no selecciona la venta que desea eliminar", "Eliminar Venta", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}

		}

	}
}
