﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void btnIrCatalogo_Click(object sender, RoutedEventArgs e)
		{
			Catalogos VerCatalogos = new Catalogos();
			VerCatalogos.Show();
			Close();

		}

		private void btnIrVentas_Click(object sender, RoutedEventArgs e)
		{
			Ventas VerVentas = new Ventas();
			VerVentas.Show();
			Close();
		}
	}
}
