﻿using FarmaciaMQE.BIZ;
using FarmaciaMQE.COMMON;
using FarmaciaMQE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmaciaMQE.UI.Escritorio
{
	/// <summary>
	/// Lógica de interacción para NuevoCliente.xaml
	/// </summary>
	public partial class NuevoCliente : Window
	{
		ManejadorDeClientes manejadorDeClientes;
		enum Accion
		{
			Nuevo,
			Editar,
		}
		Accion accionCategoria;
		Cliente c = new Cliente();
		public NuevoCliente(int AccionElemento, Cliente c)
		{
			InitializeComponent();
			txbDireccion.Clear();
			txbNombreEmpleado.Clear();
			txbApellidoPaterno.Clear();
			txbApellidoMaterno.Clear();
			txbemail.Clear();
			txbRFC.Clear();
			txbTelefono.Clear();
			manejadorDeClientes = new ManejadorDeClientes(new RepositorioDeClientes());
			switch (AccionElemento)
			{
				case 1:
					accionCategoria = Accion.Nuevo;
					btnEditar.IsEnabled = false;
					break;
				case 2:
					btnEditar.IsEnabled = true;
					btnCancelar.IsEnabled = true;
					btnLimpiar.IsEnabled = false;
					btnGuardarCliente.IsEnabled = false;
					txbNombreEmpleado.Text = c.Nombre;
					txbApellidoPaterno.Text = c.ApellidoPaterno;
					txbApellidoMaterno.Text = c.ApellidoMaterno;
					txbDireccion.Text = c.Direccion;
					txbemail.Text = c.Correo;
					txbRFC.Text = c.RFC;
					txbTelefono.Text = c.Telefono;
					txbApellidoMaterno.IsEnabled = false;
					txbApellidoPaterno.IsEnabled = false;
					txbDireccion.IsEnabled = false;
					txbemail.IsEnabled = false;
					txbNombreEmpleado.IsEnabled = false;
					txbRFC.IsEnabled = false;
					txbTelefono.IsEnabled = false;
					this.c = c;
					break;

			}
		}

		private void btnLimpiar_Click(object sender, RoutedEventArgs e)
		{
			txbDireccion.Clear();
			txbNombreEmpleado.Clear();
			txbApellidoPaterno.Clear();
			txbApellidoMaterno.Clear();
			txbemail.Clear();
			txbRFC.Clear();
			txbTelefono.Clear();
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			Catalogos VentanaCatalogos = new Catalogos();
			VentanaCatalogos.Show();
			Close();
		}

		private void btnGuardarCliente_Click(object sender, RoutedEventArgs e)
		{
			if (accionCategoria == Accion.Nuevo)
			{
				Cliente cliente = new Cliente()
				{
					Nombre = txbNombreEmpleado.Text,
					ApellidoMaterno=txbApellidoMaterno.Text,
					ApellidoPaterno=txbApellidoPaterno.Text,
					Correo=txbemail.Text,
					Direccion=txbDireccion.Text,
					RFC=txbRFC.Text,
					Telefono=txbTelefono.Text,
				};
				if (manejadorDeClientes.Agregar(cliente))
				{
					MessageBox.Show("Cliente agregado con éxito", "Nueva Cliente", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al agregar Cliente ", "Nueva Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{
				c.Nombre = txbNombreEmpleado.Text;
				c.ApellidoMaterno = txbApellidoMaterno.Text;
				c.ApellidoPaterno = txbApellidoPaterno.Text;
				c.Correo = txbemail.Text;
				c.Direccion = txbDireccion.Text;
				c.RFC = txbRFC.Text;
				c.Telefono = txbTelefono.Text;
				if (manejadorDeClientes.Modificar(c))
				{
					MessageBox.Show("Cliente modificado con éxito", "Editar Cliente", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					Catalogos Principal = new Catalogos();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al mofificar Cliente", "Editar Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
		}

		private void btnEditar_Click(object sender, RoutedEventArgs e)
		{
			accionCategoria = Accion.Editar;
			btnEditar.IsEnabled = false;
			btnCancelar.IsEnabled = true;
			btnLimpiar.IsEnabled = true;
			btnGuardarCliente.IsEnabled = true;
			txbApellidoMaterno.IsEnabled = true;
			txbApellidoPaterno.IsEnabled = true;
			txbDireccion.IsEnabled = true;
			txbemail.IsEnabled = true;
			txbNombreEmpleado.IsEnabled = true;
			txbRFC.IsEnabled = true;
			txbTelefono.IsEnabled = true;

		}
	}
}
