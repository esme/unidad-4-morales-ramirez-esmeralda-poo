﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmaciaMQE.COMMON.Entidades
{
	public class Articulo
	{
		public string Producto { get; set; }
		public string TotalArticulo { get; set; }
	}
}
