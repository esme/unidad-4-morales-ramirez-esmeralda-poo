﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmaciaMQE.COMMON
{
	public class Categoria:Base
	{
		public string Nombre { get; set; }
		public string Descripcion { get; set; }
		public override string ToString()
		{
			return string.Format("{0}", Nombre);
		}
	}
}
