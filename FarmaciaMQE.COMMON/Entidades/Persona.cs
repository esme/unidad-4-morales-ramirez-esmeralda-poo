﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmaciaMQE.COMMON
{
	public class Persona:Base
	{
		public string Nombre { get; set; }
		public string Telefono { get; set; }
		public string ApellidoPaterno{ get; set; }
		public string ApellidoMaterno { get; set; }

	}
}
