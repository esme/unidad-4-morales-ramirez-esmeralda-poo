﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmaciaMQE.COMMON.Entidades
{
	public class Venta:Base
	{
		public DateTime Fecha { get; set; }
		public string Comprador { get; set; }
		public string Vendedeor { get; set; }
		public List<Medicamento> Productos { get; set; }
		public double Total { get; set; }
	}
}
