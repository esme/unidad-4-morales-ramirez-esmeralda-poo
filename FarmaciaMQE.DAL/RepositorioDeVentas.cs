﻿using FarmaciaMQE.COMMON.Entidades;
using FarmaciaMQE.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarmaciaMQE.DAL
{
    public class RepositorioDeVentas : IRepositorio<Venta>
	{
		private string DBName = @"C:\Catalogos\Catalogos.db";
		private string TableName = "Ventas";

		public List<Venta> Read
		{
			get
			{
				List<Venta> datos = new List<Venta>();
				using (var db = new LiteDatabase(DBName))
				{
					datos = db.GetCollection<Venta>(TableName).FindAll().ToList();
				}
				return datos;
			}
		}

		public bool Create(Venta entidad)
		{
			entidad.Id = Guid.NewGuid().ToString();
			try
			{
				using (var db = new LiteDatabase(DBName))
				{
					var coleccion = db.GetCollection<Venta>(TableName);
					coleccion.Insert(entidad);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public bool Delete(string id)
		{
			try
			{
				int r;
				using (var db = new LiteDatabase(DBName))
				{
					var coleccion = db.GetCollection<Venta>(TableName);
					r = coleccion.Delete(e => e.Id == id);
				}
				return r > 0;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public bool Update(Venta entidadModificada)
		{
			try
			{
				using (var db = new LiteDatabase(DBName))
				{
					var coleccion = db.GetCollection<Venta>(TableName);
					coleccion.Update(entidadModificada);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}
	}
}
