﻿using FarmaciaMQE.COMMON;
using FarmaciaMQE.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarmaciaMQE.DAL
{
	public class RepositorioDeMedicamentos: IRepositorio<Medicamento>
    {
		private string DBName = @"C:\Catalogos\Catalogos.db";
		private string TableName = "Medicamentos";

		public List<Medicamento> Read
		{
			get
			{
				List<Medicamento> datos = new List<Medicamento>();
				using (var db = new LiteDatabase(DBName))
				{
					datos = db.GetCollection<Medicamento>(TableName).FindAll().ToList();
				}
				return datos;
			}
		}

		public bool Create(Medicamento entidad)
		{
			entidad.Id = Guid.NewGuid().ToString();
			try
			{
				using (var db = new LiteDatabase(DBName))
				{
					var coleccion = db.GetCollection<Medicamento>(TableName);
					coleccion.Insert(entidad);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public bool Delete(string id)
		{
			try
			{
				int r;
				using (var db = new LiteDatabase(DBName))
				{
					var coleccion = db.GetCollection<Medicamento>(TableName);
					r = coleccion.Delete(e => e.Id == id);
				}
				return r > 0;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public bool Update(Medicamento entidadModificada)
		{
			try
			{
				using (var db = new LiteDatabase(DBName))
				{
					var coleccion = db.GetCollection<Medicamento>(TableName);
					coleccion.Update(entidadModificada);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}
	}
}
